import Vue from 'vue'
import Router from 'vue-router'
import VueCookies from 'vue-cookies'
Vue.use(Router)
Vue.use(VueCookies)

const router = new Router({
  routes: [{
      path: '/',
      name: 'login', //登录界面
      component: () => import('@/components/common/login'),
    },

    {
      path: '/register',
      name: 'register',
      component: () => import('@/components/common/registerStudent'),
    },
    {
      path: '/loginOut',
      redirect: '/',
      //meta:{routerPath:true}
    },
    {
      path: '/index', //教师主页
      component: () => import('@/components/admin/index'),
      meta: {
        routerPath: true
      },
      children: [{
          path: '/', //首页默认路由
          component: () => import('@/components/common/hello'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/grade', //学生成绩
          component: () => import('@/components/charts/grade'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/selectExamToPart', //学生分数段
          component: () => import('@/components/teacher/selectExamToPart'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/scorePart',
          component: () => import('@/components/charts/scorePart'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/allStudentsGrade', //所有学生成绩统计
          component: () => import('@/components/teacher/allStudentsGrade'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/examDescription', //考试管理功能描述
          component: () => import('@/components/teacher/examDescription'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/selectExam', //查询所有考试
          component: () => import('@/components/teacher/selectExam'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/addExam', //添加考试
          component: () => import('@/components/teacher/addExam'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/answerDescription', //题库管理功能介绍
          component: () => import('@/components/teacher/answerDescription'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/selectAnswer', //查询所有题库
          component: () => import('@/components/teacher/selectAnswer'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/addAnswer', //增加题库主界面
          component: () => import('@/components/teacher/addAnswer'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/addAnswerChildren', //点击试卷跳转到添加题库页面
          component: () => import('@/components/teacher/addAnswerChildren'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/studentManage', //学生管理界面
          component: () => import('@/components/teacher/studentManage'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/addStudent', //添加学生
          component: () => import('@/components/teacher/addStudent'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/teacherManage',
          component: () => import('@/components/admin/tacherManage'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/addTeacher',
          component: () => import('@/components/admin/addTeacher'),
          meta: {
            routerPath: true
          },
        }
      ]
    },
    {
      path: '/student',
      meta: {
        routerPath: true
      },
      component: () => import('@/components/student/index'),

      children: [{
          path: "/",
          component: () => import('@/components/student/myExam'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/startExam',
          component: () => import('@/components/student/startExam'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/manager',
          component: () => import('@/components/student/manager'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/examMsg',
          component: () => import('@/components/student/examMsg'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/message',
          component: () => import('@/components/student/message'),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/studentScore',
          component: () => import("@/components/student/answerScore"),
          meta: {
            routerPath: true
          },
        },
        {
          path: '/scoreTable',
          component: () => import("@/components/student/scoreTable"),
          meta: {
            routerPath: true
          },
        }
      ]
    },
    {
      path: '/answer',
      component: () => import('@/components/student/answer'),
      meta: {
        routerPath: true
      },
    }
  ]
})

// router.beforeEach((to, from, next) => {
//   if(to.meta.routerPath){
//     // if(this.$cookies.get("cname")!=null ){
//     //   next();
//     // }else{
//     //   next({
//     //     path:'/'
//     //   })
//     // }
//     console.log(this.$cookies.get("cname"))
//   }else{
//     // if(this.$cookies.get("cname")!=null ){
//     //   next();
//     // }else{
//     //   next({
//     //     path:'/'
//     //   })
//     // }
//   }
// })
export default router
router.beforeEach((to, from, next) => {
  if (to.matched.some(res => res.meta.routerPath) == true) {
    if (!router.app.$cookies.get("cname")) {
      next({
        path: "/"
      })
    } else {
      next();
    }
  } else {
    next();
  }
})
