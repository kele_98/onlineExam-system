package com.exam.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * @author kele
 * @date 2021/9/14 13:56
 * @description
 */
public class RedisPage implements IPage{

    Page iPage=new Page();

    @Override
    public long getPages() {
        if (getSize() == 0) {
            return 0L;
        }
        long pages = getTotal() / getSize();
        if (getTotal() % getSize() != 0) {
            pages++;
        }
        return pages;
    }

    @Override
    public IPage setPages(long pages) {
        return iPage.setPages(pages);
    }

    @Override
    public List getRecords() {
        return iPage.getRecords();
    }

    @Override
    public IPage setRecords(List records) {
        return iPage.setRecords(records);
    }

    @Override
    public long getTotal() {
        return iPage.getTotal();
    }

    @Override
    public IPage setTotal(long total) {
        return iPage.setTotal(total);
    }

    @Override
    public long getSize() {
        return iPage.getSize();
    }

    @Override
    public IPage setSize(long size) {
        return iPage.setSize(size);
    }

    @Override
    public long getCurrent() {
        return iPage.getCurrent();
    }

    @Override
    public IPage setCurrent(long current) {
        return iPage.setCurrent(current);
    }
}
