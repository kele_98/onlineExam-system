package com.exam.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ApiResult;
import com.exam.entity.ExamManage;
import com.exam.serviceimpl.ExamManageServiceImpl;
import com.exam.util.ApiResultHandler;
import com.exam.util.RedisPage;
import com.exam.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ExamManageController {

    int start=0;
    int end;

    @Autowired
    private ExamManageServiceImpl examManageService;

    @Autowired
    private RedisUtil redisUtil;
    @GetMapping("/exams")
    public ApiResult findAll(){
        System.out.println("不分页查询所有试卷");
        ApiResult apiResult;
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", examManageService.findAll());
        return apiResult;
    }

    @GetMapping("/exams/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        System.out.println("分页查询所有试卷");
        ApiResult apiResult = null;
        Page<ExamManage> examManage = new Page<>(page,size);
        List<ExamManage> get = redisUtil.lGet("examAll", 0, -1);
        if(get.size()==0){
            IPage<ExamManage> all = examManageService.findAll(examManage);
            System.out.println(all.toString());
            apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", all);
        }else {

            /**
             * redis分页操作
             */
            List<ExamManage> examAll = (List<ExamManage>) redisUtil.lGet("examAll",0,-1);
            if (page==1){
                start=page-1;
                end=size;
            }else {
                start+=size;
                end+=size;
                if (end>examAll.size()){
                    end=examAll.size();
                }
                System.out.println(start+" ======>  "+end);
            }
            List<ExamManage> subList = examAll.subList(start, end);
            RedisPage list=new RedisPage();
            list.setRecords(subList);
            list.setTotal(examAll.size());
            list.setSize(size);
            list.setCurrent(page);
            apiResult=ApiResultHandler.buildApiResult(200, "请求成功！", list);
        }
        return apiResult;
    }

    @GetMapping("/exam/{examCode}")
    public ApiResult findById(@PathVariable("examCode") Integer examCode){
        System.out.println("根据ID查找");
        ExamManage res = examManageService.findById(examCode);
        if(res == null) {
            return ApiResultHandler.buildApiResult(10000,"考试编号不存在",null);
        }
        return ApiResultHandler.buildApiResult(200,"请求成功！",res);
    }

    @DeleteMapping("/exam/{examCode}")
    public ApiResult deleteById(@PathVariable("examCode") Integer examCode){
        int res = examManageService.delete(examCode);
        return ApiResultHandler.buildApiResult(200,"删除成功",res);
    }

    @PutMapping("/exam")
    public ApiResult update(@RequestBody ExamManage exammanage){
        int res = examManageService.update(exammanage);
//        if (res == 0) {
//            return ApiResultHandler.buildApiResult(20000,"请求参数错误");
//        }
        System.out.print("更新操作执行---");
        return ApiResultHandler.buildApiResult(200,"更新成功",res);
    }

    @PostMapping("/exam")
    public ApiResult add(@RequestBody ExamManage exammanage) throws InterruptedException {
        int res = examManageService.add(exammanage);
        if (res ==1) {
            return ApiResultHandler.buildApiResult(200, "添加成功", res);
        } else {
            return  ApiResultHandler.buildApiResult(400,"添加失败",res);
        }
    }

    @GetMapping("/examManagePaperId")
    public ApiResult findOnlyPaperId() {
        ExamManage res = examManageService.findOnlyPaperId();
        if (res != null) {
            return ApiResultHandler.buildApiResult(200,"请求成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"请求失败",res);
    }
}
