package com.exam.config;

import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author kele
 * @date 2021/9/13 10:33
 * @description
 */

public class SessionConfiguration  extends WebMvcConfigurerAdapter {

    // 注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加拦截的请求，并排除几个不拦截的请求
        registry.addInterceptor(new LoginConfig()).addPathPatterns("/**")
                .excludePathPatterns( "/", "/login");
    }
}
