package com.exam;

/**
 * @author kele
 * @date 2021/9/15 18:37
 * @description
 */
public class Test {
    public static void main(String sgf[]) {
        int [] nums={1,2,3,1};
        findPeakElement(nums);

    }
    public static int findPeakElement(int[] nums) {
        int left=0;
        int right=nums.length-1;
        int mid = (right +left) / 2;
        while (left<right){
            if(nums[mid]>nums[mid+1]){
                right=mid;
            }
            else {
                left=mid+1;
            }
        }
        return left;
    }
}
